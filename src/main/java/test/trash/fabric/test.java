package test.trash.fabric;

import net.fabricmc.api.ModInitializer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.item.ToolMaterials;

public class test implements ModInitializer {

    public static final ThiccJustin THICC_JUSTIN = new ThiccJustin(new Item.Settings().group(ItemGroup.COMBAT).fireproof().maxCount(1));
    public static final Item Bob = new BobItem(ToolMaterials.IRON, 5, -1, new Item.Settings().group(ItemGroup.COMBAT).maxCount(1).maxDamage(400));
    public static final Item RBob = new BobItem(ToolMaterials.IRON, 6, -2, new Item.Settings().group(ItemGroup.COMBAT).maxCount(1).maxDamage(600));

	@Override
	public void onInitialize() {

	Registry.register(Registry.ITEM, new Identifier("test", "thicc_justin"), THICC_JUSTIN);
        Registry.register(Registry.ITEM, new Identifier("test", "bob"), Bob);
        Registry.register(Registry.ITEM, new Identifier("test", "rbob"), RBob);

	}
}
