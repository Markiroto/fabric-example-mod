package test.trash.fabric;

import net.minecraft.item.ToolMaterial;
import net.minecraft.item.SwordItem;

public class BobItem extends SwordItem {
  public static final int REACH = 2;
  public static final int REACH_SQUARED = REACH * REACH;

  public BobItem(ToolMaterial toolMaterial, int attackDamage, float attackSpeed, Settings settings) {
    super(toolMaterial, attackDamage, attackSpeed, settings);
  }

}
