package test.trash.fabric;

// import java.util.List;

// import net.fabricmc.api.EnvType;
// import net.fabricmc.api.Environment;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
// import net.minecraft.item.ItemGroup;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;

public class ThiccJustin extends Item {

        public ThiccJustin(Settings settings)
    {
        super(settings);
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity playerEntity, Hand hand)
    {
        playerEntity.addStatusEffect(new StatusEffectInstance(StatusEffects.POISON, 3600, 0));
        playerEntity.addStatusEffect(new StatusEffectInstance(StatusEffects.NAUSEA, 3600, 0));
        playerEntity.addStatusEffect(new StatusEffectInstance(StatusEffects.UNLUCK, 3600, 0));
        playerEntity.addStatusEffect(new StatusEffectInstance(StatusEffects.MINING_FATIGUE, 3600, 0));
        playerEntity.addStatusEffect(new StatusEffectInstance(StatusEffects.HUNGER, 3600, 0));
        return new TypedActionResult<>(ActionResult.SUCCESS, playerEntity.getStackInHand(hand));
    }

}
